﻿using Money.Commands;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.IO;
using System.Runtime.CompilerServices;
using System.Windows.Input;

namespace Money
{
    public class MoneyViewModel : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler? PropertyChanged;

        void OnPropertyChanged([CallerMemberName] string? path = null)
        {
            PropertyChanged?.Invoke(this, new PropertyChangedEventArgs(path));
        }


        private int _billCount { get; set; }
        public int BillCount {
            get => _billCount;
            set { _billCount = value; OnPropertyChanged(); }
        }

        private int _moneySum { get; set; }
        public int MoneySum
        {
            get => _moneySum;
            set { _moneySum = value; OnPropertyChanged(); }
        }

        Dictionary<int, int> _billValueCount = new Dictionary<int, int>();
        public Dictionary<int, int> BillValueCount
        {
            get => _billValueCount;
            set
            {
                _billValueCount = value;
                OnPropertyChanged();
            }
        }

        private int _rest;
        public int Rest
        {
            get => _rest;
            set
            {
                _rest = value;
                OnPropertyChanged();
            }
        }


        private ICommand? _calculateSumCommand;

        public ICommand CalculateSumCommand
        {
            get
            {
                if (_calculateSumCommand == null)
                {
                    _calculateSumCommand = new CalculateSumCommand(this);
                }
                return _calculateSumCommand;
            }
            set
            {
                _calculateSumCommand = value;
                OnPropertyChanged();
            }
        }
    }
}
