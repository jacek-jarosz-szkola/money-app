﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;

namespace Money.Commands
{
    class CalculateSumCommand : ICommand
    {
        private readonly int[] billTypes = { 500, 200, 100, 50, 20, 10 };

        private MoneyViewModel vm;

        public CalculateSumCommand(MoneyViewModel vm)
        {
            this.vm = vm;
        }

        public event EventHandler? CanExecuteChanged;

        public bool CanExecute(object parameter)
        {
            return true;    
        }

        public void Execute(object parameter)
        {
            var rest = vm.MoneySum;

        


            foreach (var bill in billTypes.OrderByDescending(x => x))
            {
                int billCount = rest/bill;
                rest -= billCount * bill;
                vm.BillValueCount[bill] = billCount;
            }
            vm.Rest = rest;
        }
    }
}
