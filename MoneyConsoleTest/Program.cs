﻿using System;
using System.Collections.Generic;
using System.Security.Cryptography.X509Certificates;
using Money;

namespace MoneyConsoleTest
{
    class Program
    {
        static void Main(string[] args)
        {
            var money = new MoneyViewModel();
            money.MoneySum = 1250;
            money.CalculateSumCommand.Execute(null);
            Console.WriteLine($"Suma: {money.MoneySum}");
            foreach(KeyValuePair<int,int> element in money.BillValueCount)
            {
                Console.WriteLine($"{element.Key} x{element.Value}");
            }
        }
    }
}
